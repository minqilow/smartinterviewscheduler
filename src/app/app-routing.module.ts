import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainCalenderComponent } from './main-calender/main-calender.component';


const routes: Routes = [
  { path: '', redirectTo: '/main-calender', pathMatch: 'full'}, 
  { path: 'main-calender', component: MainCalenderComponent },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
